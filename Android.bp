/**
 * Copyright (C) 2018-2020 The Dirty Unicorns Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

soong_namespace {
}

android_app_import {
	name: "GoogleExtShared",
	owner: "pixelgapps",
	apk: "proprietary/app/GoogleExtShared/GoogleExtShared.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	overrides: ["ExtShared"],
}

android_app_import {
	name: "GooglePrintRecommendationService",
	owner: "pixelgapps",
	apk: "proprietary/app/GooglePrintRecommendationService/GooglePrintRecommendationService.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	overrides: ["PrintRecommendationService"],
}

android_app_import {
	name: "arcore",
	owner: "pixelgapps",
	apk: "proprietary/product/app/arcore/arcore.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
}

android_app_import {
	name: "GoogleContacts",
	owner: "pixelgapps",
	apk: "proprietary/product/app/GoogleContacts/GoogleContacts.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
	overrides: ["Contacts"],
}

android_app_import {
	name: "GoogleContactsSyncAdapter",
	owner: "pixelgapps",
	apk: "proprietary/product/app/GoogleContactsSyncAdapter/GoogleContactsSyncAdapter.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
}

android_app_import {
	name: "GoogleTTS",
	owner: "pixelgapps",
	apk: "proprietary/product/app/GoogleTTS/GoogleTTS.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
}

android_app_import {
	name: "LocationHistoryPrebuilt",
	owner: "pixelgapps",
	apk: "proprietary/product/app/LocationHistoryPrebuilt/LocationHistoryPrebuilt.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
}

android_app_import {
	name: "MarkupGoogle",
	owner: "pixelgapps",
	apk: "proprietary/product/app/MarkupGoogle/MarkupGoogle.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
}

android_app_import {
	name: "Ornament",
	owner: "pixelgapps",
	apk: "proprietary/product/app/Ornament/Ornament.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
}

android_app_import {
	name: "Photos",
	owner: "pixelgapps",
	apk: "proprietary/product/app/Photos/Photos.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
	overrides: ["Gallery", "Gallery2"],
}

android_app_import {
	name: "PrebuiltBugle",
	owner: "pixelgapps",
	apk: "proprietary/product/app/PrebuiltBugle/PrebuiltBugle.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
	overrides: ["messaging"],
}


android_app_import {
	name: "PrebuiltDeskClockGoogle",
	owner: "pixelgapps",
	apk: "proprietary/product/app/PrebuiltDeskClockGoogle/PrebuiltDeskClockGoogle.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
	overrides: ["DeskClock"],
}

android_app_import {
	name: "talkback",
	owner: "pixelgapps",
	apk: "proprietary/product/app/talkback/talkback.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
}

android_app_import {
	name: "TrichromeLibrary-Stub",
	owner: "pixelgapps",
	apk: "proprietary/product/app/TrichromeLibrary-Stub/TrichromeLibrary-Stub.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
}

android_app_import {
	name: "WebViewGoogle-Stub",
	owner: "pixelgapps",
	apk: "proprietary/product/app/WebViewGoogle-Stub/WebViewGoogle-Stub.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
	overrides: ["webview"],
}

android_app_import {
	name: "AndroidMigratePrebuilt",
	owner: "pixelgapps",
	apk: "proprietary/product/priv-app/AndroidMigratePrebuilt/AndroidMigratePrebuilt.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
}

android_app_import {
	name: "AndroidAutoStubPrebuilt",
	owner: "pixelgapps",
	apk: "proprietary/product/priv-app/AndroidAutoStubPrebuilt/AndroidAutoStubPrebuilt.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
}

android_app_import {
	name: "ConfigUpdater",
	owner: "pixelgapps",
	apk: "proprietary/product/priv-app/ConfigUpdater/ConfigUpdater.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
}

android_app_import {
	name: "ConnMetrics",
	owner: "pixelgapps",
	apk: "proprietary/product/priv-app/ConnMetrics/ConnMetrics.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
}

android_app_import {
	name: "GoogleDialer",
	owner: "pixelgapps",
	apk: "proprietary/product/priv-app/GoogleDialer/GoogleDialer.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
	overrides: ["Dialer"],
}

android_app_import {
	name: "OTAConfigPrebuilt",
	owner: "pixelgapps",
	apk: "proprietary/product/priv-app/OTAConfigPrebuilt/OTAConfigPrebuilt.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
}

android_app_import {
	name: "PartnerSetupPrebuilt",
	owner: "pixelgapps",
	apk: "proprietary/product/priv-app/PartnerSetupPrebuilt/PartnerSetupPrebuilt.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
}

android_app_import {
	name: "Phonesky",
	owner: "pixelgapps",
	apk: "proprietary/product/priv-app/Phonesky/Phonesky.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
}

android_app_import {
	name: "PrebuiltGmsCore",
	owner: "pixelgapps",
	apk: "proprietary/product/priv-app/PrebuiltGmsCore/PrebuiltGmsCoreRvc.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
}

android_app_import {
	name: "RecorderPrebuilt",
	owner: "pixelgapps",
	apk: "proprietary/product/priv-app/RecorderPrebuilt/RecorderPrebuilt.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
}

android_app_import {
	name: "SetupWizardPrebuilt",
	owner: "pixelgapps",
	apk: "proprietary/product/priv-app/SetupWizardPrebuilt/SetupWizardPrebuilt.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
	overrides: ["OneTimeInitializer", "Provision"],
}

android_app_import {
	name: "TurboPrebuilt",
	owner: "pixelgapps",
	apk: "proprietary/product/priv-app/TurboPrebuilt/TurboPrebuilt.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
}

android_app_import {
	name: "Velvet",
	owner: "pixelgapps",
	apk: "proprietary/product/priv-app/Velvet/Velvet.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
	overrides: ["QuickSearchBox"],
}

android_app_import {
	name: "WellbeingPrebuilt",
	owner: "pixelgapps",
	apk: "proprietary/product/priv-app/WellbeingPrebuilt/WellbeingPrebuilt.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
}

dex_import {
	name: "com.google.android.dialer.support",
	owner: "pixelgapps",
	jars: ["proprietary/product/framework/com.google.android.dialer.support.jar"],
	product_specific: true,
}

android_app_import {
	name: "GoogleFeedback",
	owner: "pixelgapps",
	apk: "proprietary/system_ext/priv-app/GoogleFeedback/GoogleFeedback.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	system_ext_specific: true,
}

android_app_import {
	name: "GoogleOneTimeInitializer",
	owner: "pixelgapps",
	apk: "proprietary/system_ext/priv-app/GoogleOneTimeInitializer/GoogleOneTimeInitializer.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	system_ext_specific: true,
	overrides: ["OneTimeInitializer"],
}

android_app_import {
	name: "GoogleServicesFramework",
	owner: "pixelgapps",
	apk: "proprietary/system_ext/priv-app/GoogleServicesFramework/GoogleServicesFramework.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	system_ext_specific: true,
}

android_app_import {
	name: "PixelSetupWizard",
	owner: "pixelgapps",
	apk: "proprietary/system_ext/priv-app/PixelSetupWizard/PixelSetupWizard.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	system_ext_specific: true,
}

android_app_import {
	name: "GBoard",
	owner: "pixelgapps",
	apk: "proprietary/product/app/GBoard/GBoard.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
	overrides: ["LatinIME"]
}

android_app_import {
	name: "GoogleCalculator",
	owner: "pixelgapps",
	apk: "proprietary/product/app/GoogleCalculator/GoogleCalculator.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
	overrides: ["Calculator", "ExactCalculator"]
}

android_app_import {
	name: "GoogleCalendar",
	owner: "pixelgapps",
	apk: "proprietary/product/app/GoogleCalendar/GoogleCalendar.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
	overrides: ["Calendar", "GoogleCalendarSyncAdapter"]
}
